FROM java:latest

COPY ./build/libs/*.jar /usr/app/

WORKDIR /usr/app

ENTRYPOINT ["java","-jar","hotel-master-service-api-0.1.0.jar"]
