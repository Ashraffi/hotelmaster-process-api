package HotelMasterProcessAPI.controller;

import HotelMasterProcessAPI.restrepository.HotelMasterProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/process/hotelMaster")
public class HotelMasterProcessController {

    @Value("${app.url}")
    public String url;

    public HotelMasterProcessRepository repo=new HotelMasterProcessRepository();
        @RequestMapping("/get")
    public  Object  GetEntity()
    {
        return repo.getHotelId(url);
    }
}
