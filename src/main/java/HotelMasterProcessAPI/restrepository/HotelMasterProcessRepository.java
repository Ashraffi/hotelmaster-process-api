package HotelMasterProcessAPI.restrepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HotelMasterProcessRepository {
    Logger logger = LoggerFactory.getLogger(HotelMasterProcessRepository.class);
    public Object getHotelId(String url)
    {
        logger.info("service url" +url);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(url, Object.class);
    }
}
